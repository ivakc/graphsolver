﻿using System;
using GraphSolver.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace GraphSolver.Core.GraphCalculator
{
    /// <summary>
    /// A static class to perform certain calculations on the given graph (which is a List of Nodes)
    /// </summary>
    public static class Calculator
    {
        /// <summary>
        /// Finds the shortest path in the given graph between the two nodes with the given ids (<paramref name="idFrom"/>, <paramref name="idTo"/>)
        /// </summary>
        /// <param name="graph">The graph, which is a List of Nodes</param>
        /// <param name="idFrom">The Id of the node to start from</param>
        /// <param name="idTo">The Id of the node to travel</param>
        /// <returns>List of strings, which are ids on the shortest path</returns>
        public static List<string> ShortestPath(List<Node> graph, string idFrom, string idTo)
        {
            List<string> shortestPath = new List<string>();

            if (graph == null || string.IsNullOrEmpty(idFrom) || string.IsNullOrEmpty(idTo))
            {
                return shortestPath;
            }

            Normalize(graph);

            // graph nodes should be unique on Id. If not, this is invalid operation
            if (graph.GroupBy(x => x.Id).Any(c => c.Count() > 1))
            {
                throw new InvalidOperationException("Graph has nodes with same Id");
            }

            Node nodeStart = graph.Find(x => x.Id == idFrom);
            if (nodeStart == null) 
            {
                return shortestPath;
            }

            Node nodeStop = graph.Find(x => x.Id == idTo);
            if (nodeStop == null)
            {
                return shortestPath;
            }

            // This can only be valid scenario if idFrom equals idTo, which is handled above
            if (graph.Count == 1)
            {
                return shortestPath;
            }

            HashSet<Node> alreadyVisited = new HashSet<Node>();
            Queue<Node> toVisit = new Queue<Node>();
            Dictionary<string, string> parent = new Dictionary<string, string>();

            alreadyVisited.Add(nodeStart);
            toVisit.Enqueue(nodeStart);

            while (toVisit.Count > 0)
            {
                Node currentNode = toVisit.Dequeue();

                // Found
                if (currentNode.Id == nodeStop.Id)
                {
                    string id = currentNode.Id;
                    while (id != null)
                    {
                        shortestPath.Add(id);
                        id = parent.ContainsKey(id) ? parent[id] : null;
                    }
                    if (nodeStop.Id == nodeStart.Id)
                    {
                        shortestPath.Add(nodeStart.Id);
                    }
                    return shortestPath;
                }
                
                if (currentNode.AdjacentNodes == null)
                {
                    continue;
                }

                foreach (string nodeId in currentNode.AdjacentNodes)
                {
                    Node adjacentNode = graph.FirstOrDefault(x => x.Id == nodeId);
                    if (adjacentNode != null && !alreadyVisited.Contains(adjacentNode))
                    {
                        parent.Add(nodeId, currentNode.Id);
                        alreadyVisited.Add(adjacentNode);
                        toVisit.Enqueue(adjacentNode);
                    }
                }
            }
            
            // Not found
            return new List<string>();
        }

        /// <summary>
        /// Nodes have adjacency list. In bidirectional representation, the edges should be present in both nodes' adjacency lists
        /// This method modifies the input graph and normalizes it by making bidirectional
        /// </summary>
        /// <param name="graph">List of nodes to make bidirectional</param>
        private static void Normalize(List<Node> graph)
        {
            if (graph == null)
            {
                return;
            }

            foreach (Node node in graph)
            {
                if (node.AdjacentNodes == null)
                {
                    continue;
                }

                for (int j = 0; j < node.AdjacentNodes.Count; j++)
                {
                    var node1 = node;
                    Node adjacentNode = graph.Find(x => x.Id == node1.AdjacentNodes[j]);
                    if (adjacentNode == null)
                    {
                        continue;
                    }

                    if (adjacentNode.AdjacentNodes.All(x => x != node.Id))
                    {
                        adjacentNode.AdjacentNodes.Add(node.Id);
                    }
                }
            }
            
        }

        /// <summary>
        /// Finds the shortest path in the given graph between the two nodes)
        /// </summary>
        /// <param name="nodes">The graph, which is a List of Nodes</param>
        /// <param name="nodeFrom">The node to start from</param>
        /// <param name="nodeTo">The node to travel</param>
        /// <returns>List of strings, which are ids on the shortest path</returns>
        public static List<string> ShortestPath(List<Node> nodes, Node nodeFrom, Node nodeTo)
        {
            if (nodeFrom == null || nodeTo == null)
            {
                return new List<string>();
            }

            return ShortestPath(nodes, nodeFrom.Id, nodeTo.Id);
        }
    }
}
