﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GraphSolver.Core.Domain
{
    /// <summary>
    /// Represents a node in the graph
    /// </summary>
    [DataContract]
    public class Node
    {
        /// <summary>
        /// A unique string identifying the node
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// An optional human readable text which describes the node
        /// </summary>
        [BsonElement]
        [DataMember]
        public string Label { get; set; }

        /// <summary>
        /// List of ids of the nodes adjacent to current node
        /// </summary>
        [BsonElement]
        [DataMember]
        public List<string> AdjacentNodes { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Node"/> class
        /// </summary>
        /// <param name="id">A unique string identifying the node Id. Value can't be empty or null</param>
        /// <param name="label">Optional human readable text which describes the node</param>
        /// <param name="adjacentNodes">Ids of the nodes adjacent to current node</param>
        public Node(string id, string label, IEnumerable<string> adjacentNodes)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            Id = id;
            Label = label ?? string.Empty;

            AdjacentNodes = new List<string>();
            if (adjacentNodes != null)
            {
                AdjacentNodes = adjacentNodes.Where(x => !string.IsNullOrEmpty(x))
                                             .Distinct(StringComparer.CurrentCultureIgnoreCase)
                                             .ToList();
            }
        }
    }
}
