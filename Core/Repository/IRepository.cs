﻿using GraphSolver.Core.Domain;
using System.Collections.Generic;

namespace GraphSolver.Core.Repository
{
    /// <summary>
    /// Repository of <see cref="Node"/>
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Returns all Nodes in the repository
        /// </summary>
        /// <returns><see cref="List{Node}"/></returns>
        List<Node> GetAll();

        /// <summary>
        /// Returns Node with the given id
        /// </summary>
        /// <param name="id">Unique Id of Node to be retrieved</param>
        /// <returns><see cref="Node"/></returns>
        Node GetById(string id);

        /// <summary>
        /// Inserts a Node to the repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        bool Insert(Node node);

        /// <summary>
        /// Updates the given Node in the repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        bool Update(Node node);

        /// <summary>
        /// Deletes the given Node in the repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        bool Delete(Node node);

        /// <summary>
        /// Deletes all Nodes in the repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        bool DeleteAll();
    }
}
