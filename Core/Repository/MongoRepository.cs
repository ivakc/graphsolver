﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using log4net;
using System.Linq;
using GraphSolver.Core.Domain;

namespace GraphSolver.Core.Repository
{
    public class MongoRepository: IRepository
    {
        private readonly IMongoCollection<Node> _collection;
        private readonly ILog _log;

        public MongoRepository(ILog log, string connectionString, string databaseName, string collectionName)
        {
            if (log == null)
            {
                throw new ArgumentNullException(nameof(log));
            }
            _log = log;

            if (string.IsNullOrEmpty(connectionString))
            {
                _log.Error($"{nameof(connectionString)} is missing" );
                throw new ArgumentNullException(nameof(connectionString));
            }

            if (string.IsNullOrEmpty(databaseName))
            {
                _log.Error($"{nameof(databaseName)} is missing");
                throw new ArgumentNullException(nameof(databaseName));
            }

            if (string.IsNullOrEmpty(collectionName))
            {
                _log.Error($"{nameof(collectionName)} is missing");
                throw new ArgumentNullException(nameof(collectionName));
            }

            if (string.IsNullOrEmpty(connectionString))
            {
                const string ERROR_MESSAGE = "MongoDB connection string was not found in app.config";
                _log.Error(ERROR_MESSAGE);
                throw new InvalidOperationException(ERROR_MESSAGE);
            }

            MongoUrl mongoUrl = new MongoUrl(connectionString);
            MongoClient client = new MongoClient(mongoUrl);
            IMongoDatabase database = client.GetDatabase(databaseName);

            _collection = database.GetCollection<Node>(collectionName);
        }

        public List<Node> GetAll()
        {
            return _collection.Find(x => true).ToList();
        }

        public Node GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            FilterDefinition<Node> filter = Builders<Node>.Filter.Eq("_id", id);
            return _collection.Find(filter).FirstOrDefault();
        }

        public bool Insert(Node node)
        {
            if (node == null)
            {
                _log.Warn("Node is null, it won't be inserted");
                return false;
            }

            if (string.IsNullOrEmpty(node.Id))
            {
                _log.Warn("Node Id is null, it won't be inserted");
                return false;
            }

            Node existingNode = GetById(node.Id);
            if (existingNode != null)
            {
                _log.Warn("A node with same Id already exists, it won't be inserted");
                return false;
            }

            // default to empty array
            if (node.AdjacentNodes == null)
            {
                node.AdjacentNodes = new List<string>();
            }

            node.AdjacentNodes = node.AdjacentNodes.Distinct().ToList();

            _collection.InsertOne(node);
            return true;
        }

        public bool Update(Node node)
        {
            if (node == null)
            {
                _log.Warn("Node is null, it won't be updated");
                return false;
            }

            var filter = Builders<Node>.Filter.Eq("_id", node.Id);
            return _collection.ReplaceOne(filter, node).IsAcknowledged;
        }

        public bool Delete(Node node)
        {
            if (node == null)
            {
                _log.Warn("Node is null, it won't be deleted");
                return false;
            }

            var filter = Builders<Node>.Filter.Eq("_id", node.Id);
            return _collection.DeleteOne(filter).IsAcknowledged;
        }

        public bool DeleteAll()
        {
            return _collection.DeleteMany(x => true).IsAcknowledged;
        }

        public void DropCollection(string collectionName)
        {
            _collection.Database.DropCollection(collectionName);
        }
    }
}
