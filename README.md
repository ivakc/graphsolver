# GRAPH SOLVER #

An application to store a graph, display it visually and find the shortest path between selected nodes
### How do I build the project? ###

* Add the following Nuget Package Source: https://api.nuget.org/v3/index.json 
* The code was written in C# 6 so you need Visual Studio 2015 or above 

### How do I run the project and unit tests? ###

* **Run MongoDB**: MongoDB executable is included in the project, under Database folder. Run the batch file in the folder: *\Database\startMongo.bat*
* Set GraphServices as startup project. Click on GraphClient.aspx and Run/Start Debugging/F5. This should run the UI (web page) and WCF services.
* With this setup you should be able to see the graph in the page, calculate shortest path and at the same time run DataLoader (see below)
* **DataLoader** is the console application, located under the folder with same name. It performs a simple subset of CRUD operations on Repository. It can be run from command line.
* All tests should run if MongoDB is running

### Project Structure ###
* Core *(low level components)*
    * Domain *(domain objects)*
    * Repository *(data storage)*
    * GraphCalculator *(shortest path calculator)*
* Database
    * MongoDB executables
* DataLoader
    * DataLoader (**executable**) *(Console Application to get/insert/delete nodes)*
* Services
    * GraphServices *Services and web page (client)*
        * NodeService: Service for data operations
        * AjaxNodeService: Service that returns the graph to the client
        * GraphCalculationService: Service for shortest path calculation
        * GraphClient.aspx: (**executable**) The web page to interact with graph (Client)

### Highlights/Notes ###

* Unit tests use Moq library to Mock Repository
* log4net is used for logging, currently it's configured to output to Console only
