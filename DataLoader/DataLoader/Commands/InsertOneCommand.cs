﻿using DataLoader.NodeService;
using DataLoader.Serialization;
using System;
using System.IO;
using System.Xml;

namespace DataLoader.Commands
{
    /// <summary>
    /// Command to insert a single node from an xml file
    /// </summary>
    class InsertOneCommand : IShellCommand
    {
        public string GetDescription()
        {
            return "Insert a single node from an xml file, discard if already exists";
        }

        public bool Invoke()
        {
            Console.Write("Enter the path of the file: ");
            string input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            string path = Path.GetFullPath(input);
            if (!File.Exists(path))
            {
                Console.WriteLine($"File doesn't exist: \"{path}\". Press any key to continue");
                Console.ReadKey(true);
                return false;
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            Node node = new Node();
            node.LoadFromXml(doc);

            NodeServiceClient client = new NodeServiceClient();
            var isInserted = client.Insert(node);

            if (isInserted)
            {
                Console.WriteLine("Inserted. Press any key to continue");
                Console.ReadKey(true);
            }

            return isInserted;
        }
    }
}
