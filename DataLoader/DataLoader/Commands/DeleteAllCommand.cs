﻿using DataLoader.NodeService;
using System;

namespace DataLoader.Commands
{
    /// <summary>
    /// Command to delete all nodes in web service
    /// </summary>
    class DeleteAllCommand: IShellCommand
    {
        public string GetDescription()
        {
            return "Delete all nodes";
        }

        public bool Invoke()
        {
            Console.WriteLine("This will delete all nodes. Are you sure? Y/N");
            string input = Console.ReadLine();

            if (string.IsNullOrEmpty(input) || input.ToLowerInvariant() != "y")
            {
                return false;
            }

            NodeServiceClient client = new NodeServiceClient();
            bool isDeleted = client.DeleteAll();

            if (!isDeleted)
            {
                Console.WriteLine("Deletion didn't succeed. Press any key to continue...");
                Console.ReadKey(true);
                return false;
            }

            Console.WriteLine("All nodes were deleted. Press any key to continue...");
            Console.ReadKey(true);
            return true;
        }
    }
}
