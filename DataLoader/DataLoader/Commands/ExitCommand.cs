﻿using System;

namespace DataLoader.Commands
{
    class ExitCommand: IShellCommand
    {
        public string GetDescription()
        {
            return "Exit";
        }

        public bool Invoke()
        {
            Environment.Exit(0);
            return true;
        }
    }
}
