﻿using DataLoader.NodeService;
using DataLoader.Serialization;
using System;
using System.IO;
using System.Xml;

namespace DataLoader.Commands
{
    /// <summary>
    /// Command to insert all xml files under a folder as nodes
    /// </summary>
    class InsertAllCommand : IShellCommand
    {
        public string GetDescription()
        {
            return "Insert all xml files from a folder (removes all existing nodes)";
        }

        public bool Invoke()
        {
            Console.WriteLine("Enter the path of the folder (e.g. \"C:\\InputFolder\" or \"..\\Input\\");
            string input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            string path = Path.GetFullPath(input);
            if (!Directory.Exists(path))
            {
                Console.WriteLine($"Directory doesn't exist: \"{path}\". Press any key to continue");
                Console.ReadKey(true);
                return false;
            }

            NodeServiceClient client = new NodeServiceClient();
            bool isDeleted = client.DeleteAll();

            if (!isDeleted)
            {
                Console.WriteLine("The existing nodes could not be deleted. Operation will not continue. Press any key...");
                Console.ReadKey(true);
                return false;
            }

            foreach (string file in Directory.EnumerateFiles(path, "*.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(file);
                Node node = new Node();
                node.LoadFromXml(doc);

                client.Insert(node);
            }

            Console.WriteLine("Inserted. Press any key to continue");
            Console.ReadKey(true);

            return true;
        }
    }
}
