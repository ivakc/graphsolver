﻿using DataLoader.NodeService;
using System;
using System.Collections.Generic;

namespace DataLoader.Commands
{
    /// <summary>
    /// Command to return all nodes in web service
    /// </summary>
    internal class GetAllCommand: IShellCommand
    {        
        public string GetDescription()
        {
            return "List all nodes";
        }

        public bool Invoke()
        {
            NodeServiceClient client = new NodeServiceClient();
            List<Node> nodes = client.GetAll();

            foreach (Node node in nodes)
            {
                Console.Write($"Id ({node.Id}), Label ({node.Label})");

                List<string> adjacents = node.AdjacentNodes ?? new List<string>();
                Console.Write(", Adjacent Nodes (" );

                foreach (var adj in adjacents)
                {
                    Console.Write($"{adj} ");
                }
                Console.WriteLine(")");
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey(true);

            return true;
        }
    }
}
