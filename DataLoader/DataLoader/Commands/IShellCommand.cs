﻿namespace DataLoader.Commands
{
    /// <summary>
    /// Interface for the commands DataLoader shell supports
    /// </summary>
    interface IShellCommand
    {
        /// <summary>
        /// User friendly description of the command
        /// </summary>
        string GetDescription();

        /// <summary>
        /// The action command should perform
        /// </summary>
        bool Invoke();
    }
}
