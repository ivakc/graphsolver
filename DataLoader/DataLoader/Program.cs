﻿using DataLoader.Commands;
using DataLoader.NodeService;
using System;
using System.Collections.Generic;

namespace DataLoader
{
    internal class Program
    {
        public static List<IShellCommand> Commands;

        private static void Main()
        {
            CheckWebService();

            GenerateCommands();
            
            while(true)
            {
                Console.WriteLine();
                Console.WriteLine("What would you like to do? ");

                for (int i = 0; i < Commands.Count; i++)
                {
                    Console.WriteLine($"[{i+1}] {Commands[i].GetDescription()}"); // +1 is there in order not to have option "0"
                }

                string userSelection = Console.ReadLine();
                RunCommand(userSelection);
            }
        }

        private static void RunCommand(string userSelection)
        {
            int selection;
            if (string.IsNullOrEmpty(userSelection) || !int.TryParse(userSelection, out selection))
            {
                return;
            }

            if (selection < 1 || selection > Commands.Count)
            {
                return;
            }

            Commands[selection-1].Invoke(); // -1 is there in order not to have option "0"
        }

        private static void GenerateCommands()
        {
            Commands = new List<IShellCommand>
            {
                new InsertAllCommand(),
                new InsertOneCommand(),
                new GetAllCommand(),
                new DeleteAllCommand(),
                new ExitCommand()
            };

        }

        private static void CheckWebService()
        {
            try
            {
                NodeServiceClient client = new NodeServiceClient();
                client.Heartbeat();
            }
            catch
            {
                Console.WriteLine("The service is not running. Please make sure it's running and then rerun the application.");
                Console.Write("Press any key to exit...");
                Console.ReadKey(true);
                Environment.Exit(0);
            }
        }
    }
}
