﻿using DataLoader.NodeService;
using System.Collections.Generic;
using System.Xml;

namespace DataLoader.Serialization
{
    /// <summary>
    /// Extension Methods to the DataLoader.NodeService.Node class (Auto generated class of WCF)
    /// </summary>
    internal static class NodeExtensions
    {
        /// <summary>
        /// Sets the fields of the Node from the XMLDocument
        /// </summary>
        /// <param name="node"></param>
        /// <param name="xmlDoc"></param>
        internal static void LoadFromXml(this Node node, XmlDocument xmlDoc)
        {
            if (xmlDoc?.DocumentElement == null)
            {
                return;
            }

            XmlNode nodeId = xmlDoc.DocumentElement.SelectSingleNode("/node/id");
            XmlNode nodeLabel = xmlDoc.DocumentElement.SelectSingleNode("/node/label");
            XmlNodeList adjacentNodes = xmlDoc.DocumentElement.SelectNodes("/node/adjacentNodes/id");

            if (string.IsNullOrEmpty(nodeId?.InnerText))
            {
                return;
            }

            List<string> adjacents = new List<string>();
            if (adjacentNodes != null)
            {
                for (int i = 0; i < adjacentNodes.Count; i++)
                {
                    if (!string.IsNullOrEmpty(adjacentNodes[i].InnerText) && adjacentNodes[i].InnerText.Length > 0)
                    {
                        adjacents.Add(adjacentNodes[i].InnerText);
                    }
                }
            }

            node.Id = nodeId.InnerText.Trim();
            if (nodeLabel != null) node.Label = nodeLabel.InnerText.Trim();
            node.AdjacentNodes = adjacents;
        }
    }
}
