﻿using System;
using System.Collections.Generic;
using GraphSolver.Core.Domain;
using GraphSolver.Core.GraphCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GraphSolver.Tests.GraphCalculatorTests
{
    [TestClass]
    public class CalculatorTests
    {
        private static List<Node> _graphTest;

        /// <summary>
        /// Initialize the graph object for tests
        /// </summary>
        /// <param name="context"></param>
        [ClassInitialize]
        public static void InitializeEnvironment(TestContext context)
        {
            _graphTest = new List<Node>();

            for (int i = 1; i < 20; i++)
            {
                List<string> adjacentNodes = new List<string>() { ((i + 5) % 20).ToString(), ((i - 3) % 20).ToString() };
                Node newNode = new Node(i.ToString(), $"label for {i}", adjacentNodes);
                _graphTest.Add(newNode);
            }

        }

        [TestMethod]
        public void Test_NullNodes()
        {
            List<string> shortestPath = Calculator.ShortestPath(new List<Node>(), (Node)null, (Node)null);
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        public void Test_NullIds()
        {
            List<string> shortestPath = Calculator.ShortestPath(new List<Node>(), (string)null, (string)null);
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        public void Test_EmptyIds()
        {
            List<string> shortestPath = Calculator.ShortestPath(new List<Node>(), "", "");
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        public void Test_NullGraph()
        {
            List<string> shortestPath = Calculator.ShortestPath(null, "1", "2");
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        public void Test_FromNodeNotInGraph()
        {
            List<string> shortestPath = Calculator.ShortestPath(_graphTest, (_graphTest.Count + 3).ToString(), (_graphTest.Count - 1).ToString());
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        public void Test_ToNodeNotInGraph()
        {
            List<string> shortestPath = Calculator.ShortestPath(_graphTest, (_graphTest.Count - 1).ToString(), (_graphTest.Count + 3).ToString());
            Assert.IsTrue(shortestPath.Count == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_FromNodeMultipleInGraph()
        {
            List<Node> incorrectGraph = new List<Node>(_graphTest);
            incorrectGraph.Add(_graphTest[0]);
            Calculator.ShortestPath(incorrectGraph, _graphTest[0].Id, _graphTest[1].Id);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_ToNodeMultipleInGraph()
        {
            List<Node> incorrectGraph = new List<Node>(_graphTest);
            incorrectGraph.Add(_graphTest[1]);
            Calculator.ShortestPath(incorrectGraph, _graphTest[0].Id, _graphTest[1].Id);
        }

        [TestMethod]
        public void Test_GraphWithTwoNodes()
        {
            Node nodeFrom = new Node("1", "", new List<string> { "2" });
            Node nodeTo = new Node("2", "", new List<string> { "1" });
            List<Node> graph = new List<Node>() {nodeFrom, nodeTo};

            List<string> shortestPath = Calculator.ShortestPath(graph, nodeFrom, nodeTo);
            Assert.IsTrue(shortestPath.Count == 2);

        }

        [TestMethod]
        public void Test_ScrewGraph()
        {
            List<Node> graph = new List<Node>();
            for (int i = 0; i < 250; i++)
            {
                Node node = new Node(i.ToString(), "", new List<string> { (i + 1).ToString() });
                graph.Add(node);
            }
            
            List<string> shortestPath = Calculator.ShortestPath(graph, "0", "249");
            Assert.IsTrue(shortestPath.Count == 250);

        }

        [TestMethod]
        public void Test_Unconnected()
        {
            List<Node> graph = new List<Node>();
            for (int i = 0; i < 250; i++)
            {
                Node node = new Node(i.ToString(), "", new List<string> { (i + 1).ToString() });
                graph.Add(node);
            }

            Node lonelyNode = new Node("260", "", null);
            graph.Add(lonelyNode);

            List<string> shortestPath = Calculator.ShortestPath(graph, "0", "250");
            Assert.IsTrue(shortestPath.Count == 0);

        }

        [TestMethod]
        public void Test_SelfReference()
        {
            List<Node> graph = new List<Node>();
            for (int i = 0; i < 5; i++)
            {
                Node node = new Node(i.ToString(), "", new List<string> { (i + 1).ToString() });
                graph.Add(node);
            }

            Node selfRefNode = new Node("10", "", new List<string>() {"10"});
            graph.Add(selfRefNode);

            List<string> shortestPath = Calculator.ShortestPath(graph, "10", "10");
            Assert.IsTrue(shortestPath.Count == 2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_GraphWithDuplicate()
        {
            List<Node> graph = new List<Node>();
            graph.Add(new Node("1", "", new List<string>()));
            graph.Add(new Node("2", "", new List<string>() { "1", "3", "8" }));
            graph.Add(new Node("3", "", new List<string>() { "1", "9" }));
            graph.Add(new Node("1", "", new List<string>() { "14", "3", "5", "99" }));

            Calculator.ShortestPath(graph, "2", "3");
        }

        [TestMethod]
        public void Test_ShortestPath()
        {
            List<Node> graph = new List<Node>();
            
            graph.Add(new Node("1", "", new List<string>() ));
            graph.Add(new Node("2", "", new List<string>() { "1", "3", "8" }));
            graph.Add(new Node("3", "", new List<string>() { "1", "9" }));
            graph.Add(new Node("4", "", new List<string>() { "14", "3", "5", "99" }));
            graph.Add(new Node("5", "", new List<string>() { "4", "11", "6" }));
            graph.Add(new Node("6", "", new List<string>() ));
            graph.Add(new Node("7", "", new List<string>() { "6", "12", "17" }));
            graph.Add(new Node("8", "", new List<string>() { "13", "2" }));
            graph.Add(new Node("9", "", new List<string>() { "9" }));
            graph.Add(new Node("10", "", new List<string>() { "1", "16" }));
            graph.Add(new Node("11", "", new List<string>() { "6", "13" }));
            graph.Add(new Node("12", "", new List<string>() { "2", "13" }));
            graph.Add(new Node("13", "", new List<string>() ));
            graph.Add(new Node("14", "", new List<string>() { "15" }));
            graph.Add(new Node("15", "", new List<string>() { "16", "6" }));
            graph.Add(new Node("16", "", new List<string>() { "19", "10", "15" }));
            graph.Add(new Node("17", "", new List<string>() { "19", "18"}));
            graph.Add(new Node("18", "", new List<string>() { "19", "18" }));
            graph.Add(new Node("19", "", new List<string>() { "17" }));
            graph.Add(new Node("20", "", new List<string>() { "18", "8" }));
            graph.Add(new Node("21", "", new List<string>() { "17" }));

            List<string> shortestPath = Calculator.ShortestPath(graph, "1", "21");
            Assert.IsTrue(shortestPath.Count == 6); // 1 2 12 7 17 21
        }

    }
}
