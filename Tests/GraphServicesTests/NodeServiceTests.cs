﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphSolver.Core.Domain;
using GraphSolver.Core.Repository;
using Moq;
using System.Collections.Generic;
using GraphServices;
using System.Linq;

namespace GraphSolver.Tests.GraphServicesTests
{
    /// <summary>
    /// Unit Tests for NodeService. Mocks repository using Moq library and calls service methods
    /// </summary>
    [TestClass]
    public class NodeServiceTests
    {
        public static IRepository Repository;
        public static List<Node> MockNodes;

        [ClassInitialize]
        public static void InitializeEnvironment(TestContext context)
        {
            MockNodes = new List<Node>();

            Mock<IRepository> _mockNodeRepository = new Mock<IRepository>();

            _mockNodeRepository.Setup(mr => mr.GetAll()).Returns(MockNodes);

            _mockNodeRepository.Setup(mr => mr.GetById(It.IsAny<string>())).Returns((string s) => MockNodes.FirstOrDefault(x => x.Id == s));

            _mockNodeRepository.Setup(mr => mr.Insert(It.IsAny<Node>())).Returns(
                (Node node) =>
                {
                    MockNodes.Add(node);
                    return true;
                });

            _mockNodeRepository.Setup(mr => mr.Update(It.IsAny<Node>())).Returns(
                (Node node) =>
                {
                    MockNodes.RemoveAll(x => x.Id == node.Id);
                    MockNodes.Add(node);
                    return true;
                });

            _mockNodeRepository.Setup(mr => mr.Delete(It.IsAny<Node>())).Returns(
                (Node node) =>
                {
                    MockNodes.RemoveAll(x => x.Id == node.Id);
                    return true;
                });

            _mockNodeRepository.Setup(mr => mr.DeleteAll()).Returns(() => { MockNodes.Clear(); return true; });
            Repository = _mockNodeRepository.Object;
        }

        [TestMethod]
        public void Test_Heartbeat()
        {
            NodeService nodeService = new NodeService();
            Assert.IsTrue(nodeService.Heartbeat());
        }

        [TestMethod]
        public void Test_DeleteAll()
        {
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;
            bool isDeleted = nodeService.DeleteAll();

            Assert.IsTrue(isDeleted && MockNodes.Count == 0);
        }

        [TestMethod]
        public void Test_Insert()
        {
            Node node = new Node("test_id", "test_label", null);
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;
            bool isInserted = nodeService.Insert(node);

            Assert.IsTrue(isInserted && MockNodes.Count > 0);
        }

        [TestMethod]
        public void Test_GetAll()
        {
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;
            List<Node> allNodes = nodeService.GetAll();

            Assert.IsTrue(allNodes.Count == MockNodes.Count);
        }

        [TestMethod]
        public void Test_GetById()
        {
            const string ID = "test_id_Test_GetById"; 

            Node node = new Node(ID, "test_label", null);
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;
            bool isInserted = nodeService.Insert(node);

            Node returnedNode = nodeService.GetById(ID);

            Assert.IsTrue(isInserted && returnedNode != null);
        }

        [TestMethod]
        public void Test_Update()
        {
            const string ID = "test_id_Test_Update";
            const string LABEL_BEFORE_UPDATE = "label pre";
            const string LABEL_AFTER_UPDATE = "label post";

            Node node = new Node(ID, LABEL_BEFORE_UPDATE, null);
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;
            bool isInserted = nodeService.Insert(node);

            node.Label = LABEL_AFTER_UPDATE;
            bool isUpdated = nodeService.Update(node);

            Node returnedNode = nodeService.GetById(ID);

            Assert.IsTrue(isUpdated && returnedNode != null && returnedNode.Label == LABEL_AFTER_UPDATE);
        }


        [TestMethod]
        public void Test_Delete()
        {
            const string ID = "test_id_Test_Delete";
            Node node = new Node(ID, "test_label", null);
            NodeService nodeService = new NodeService();
            nodeService.Repository = Repository;

            bool isInserted = nodeService.Insert(node);
            bool isDeleted = nodeService.Delete(node);

            Node returnedNode = nodeService.GetById(ID);
            
            Assert.IsTrue(isInserted && isDeleted && returnedNode == null);
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            MockNodes.Clear();
            MockNodes = null;
            Repository = null;
        }
    }
}
