﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphSolver.Core.Domain;
using System.Collections.Generic;

namespace GraphSolver.Tests.DomainTests
{
    [TestClass]
    public class NodeTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_NullId()
        {
            Node node = new Node(null, "label", new List<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_EmptyId()
        {
            Node node = new Node(string.Empty, "label", new List<string>());
        }

        [TestMethod]
        public void Test_ValidInput()
        {
            List<string> adjacentNodes = new List<string>() {"One", "one", "", null, "One", "two" };
            Node node = new Node("2", "label", adjacentNodes);
            Assert.AreEqual(2, node.AdjacentNodes.Count);
        }
    }
}
