﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphSolver.Core.Repository;
using log4net;
using GraphSolver.Core.Domain;
using System.Collections.Generic;

namespace GraphSolver.Tests.RepositoryTests
{
    /// <summary>
    /// Tests for MongoDB repository. 
    /// Inserts/updates/deletes data to a temporary collection, and in the end drops the temporary collection. 
    /// </summary>
    [TestClass]
    public class MongoRepositoryTests
    {
        private static ILog _log;
        private const string CONNECTION_STRING = "mongodb://localhost:27017/graphsolver";
        private const string DATABASE = "graphsolver";
        private const string COLLECTION = "nodes_test";
        private const string TEST_NODE_IDENTIFIER = "test_node_identifier";
        private static List<Node> _testNodes;

        [ClassInitialize]
        public static void InitializeEnvironment(TestContext context)
        {
            _log = LogManager.GetLogger(typeof(MongoRepositoryTests));
            _testNodes = new List<Node>();
        }

        [TestMethod]
        public void Test_Insert()
        {
            Node node = CreateTestNode();
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isInserted = repository.Insert(node);

            if (isInserted)
            {
                _testNodes.Add(node); // to cleanup later
            }

            Assert.IsTrue(isInserted);
        }

        [TestMethod]
        public void Test_NullInsert()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isInserted = repository.Insert(null);
            Assert.IsFalse(isInserted);
        }

        [TestMethod]
        public void Test_Delete()
        {
            Node node = CreateTestNode();
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isInserted = repository.Insert(node);

            bool isDeleted = false; 
            if (isInserted)
            {
                _testNodes.Add(node); // to cleanup later
                isDeleted = repository.Delete(node);

                if (isDeleted)
                {
                    _testNodes.Remove(node);
                }
            }

            Assert.IsTrue(isDeleted);
        }

        [TestMethod]
        public void Test_NullDelete()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isDeleted = repository.Delete(null);
            Assert.IsFalse(isDeleted);
        }

        [TestMethod]
        public void Test_DeleteAll()
        {
            // Make sure there's at least one node in DB
            Node node = CreateTestNode();
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isInserted = repository.Insert(node);

            bool isDeleted = false;
            if (isInserted)
            {
                _testNodes.Add(node); // to cleanup later
                isDeleted = repository.DeleteAll();

                if (isDeleted)
                {
                    _testNodes.Remove(node);
                }
            }

            Assert.IsTrue(isDeleted);
        }

        [TestMethod]
        public void Test_Update()
        {
            const string TEST_ID = "test_id";
            Node node = CreateTestNode();
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isInserted = repository.Insert(node);

            bool isUpdated = false;
            if (isInserted)
            {
                _testNodes.Add(node); // to cleanup later

                node.AdjacentNodes.Add(TEST_ID);
                isUpdated = repository.Update(node);
                if (isUpdated)
                {
                    Node selectedNode = repository.GetById(node.Id);
                    isUpdated = selectedNode.AdjacentNodes.Contains(TEST_ID);
                }
            }

            Assert.IsTrue(isUpdated);
        }

        [TestMethod]
        public void Test_NullUpdate()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            bool isUpdated = repository.Update(null);
            Assert.IsFalse(isUpdated);
        }

        [TestMethod]
        public void Test_GetAll()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);

            // Just make sure there are at least 2 documents in DB
            for (int i = 0; i < 2; i++)
            {
                Node node = CreateTestNode();
                bool isInserted = repository.Insert(node);
                if (isInserted)
                {
                    _testNodes.Add(node); // to cleanup later
                }
            }
            
            List<Node> nodes = repository.GetAll();
            
            Assert.IsTrue(nodes.Count >= 2);
        }

        [TestMethod]
        public void Test_GetById()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            Node node = CreateTestNode();

            Node selectedNode = null;
            bool isInserted = repository.Insert(node);
            if (isInserted)
            {
                _testNodes.Add(node); // to cleanup later
                selectedNode = repository.GetById(node.Id);
            }

            Assert.IsTrue(isInserted && selectedNode != null && selectedNode.Label == node.Label && selectedNode.AdjacentNodes.Count == node.AdjacentNodes.Count);
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            MongoRepository repository = new MongoRepository(_log, CONNECTION_STRING, DATABASE, COLLECTION);
            repository.DropCollection(COLLECTION);
        }

        private Node CreateTestNode()
        {
            string temporaryId = Guid.NewGuid().ToString("N") + TEST_NODE_IDENTIFIER;
            return new Node(temporaryId, "test node", new List<string>() { "1", "2" });
        }

    }
}
