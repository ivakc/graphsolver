﻿using GraphSolver.Core.Domain;
using GraphSolver.Core.Repository;
using log4net;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace GraphServices
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AjaxNodeService
    {
        private IRepository _repository;
        private readonly ILog _log = LogManager.GetLogger(typeof(NodeService));

        public IRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    // Default to MongoRepository
                    string connectionString = ConfigurationManager.AppSettings["MongoDBConnString"];
                    string databaseName = ConfigurationManager.AppSettings["MongoDBName"];
                    string collectionName = ConfigurationManager.AppSettings["MongoDBCollectionName"];

                    if (!string.IsNullOrEmpty(connectionString) &&
                        !string.IsNullOrEmpty(databaseName) &&
                        !string.IsNullOrEmpty(collectionName))
                    {
                        _repository = new MongoRepository(_log, connectionString, databaseName, collectionName);
                    }
                }
                return _repository;
            }
            set
            {
                _repository = value;
            }
        }

        /// <summary>
        /// Returns all nodes from repository
        /// Removes duplicate edge representations since the result of this web service is used directly in the client to represent the graph
        /// </summary>
        /// <returns></returns>
        [WebGet]
        public List<Node> GetAll()
        {
            List<Node> nodes = Repository.GetAll();

            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].AdjacentNodes == null)
                {
                    continue;
                }

                nodes[i].AdjacentNodes = nodes[i].AdjacentNodes.Distinct().ToList();

                for (int j = 0; j < nodes[i].AdjacentNodes.Count; j++)
                {
                    if (nodes[i].AdjacentNodes[j] == nodes[i].Id) // Do not remove self reference
                    {
                        continue;
                    }

                    Node dupNode = nodes.Find(x => x.Id == nodes[i].AdjacentNodes[j]);
                    if (dupNode.AdjacentNodes == null)
                    {
                        continue;
                    }
                    dupNode.AdjacentNodes.RemoveAll(x => x == nodes[i].Id);
                }

            }
            return nodes;
        }

    }
}
