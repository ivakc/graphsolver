﻿using GraphSolver.Core.Domain;
using System.Collections.Generic;
using System.ServiceModel;

namespace GraphServices
{
    /// <summary>
    /// Set of data management services to operate on graph repository
    /// </summary>
    [ServiceContract]
    public interface INodeService
    {
        /// <summary>
        /// Returns all nodes in the repository
        /// </summary>
        /// <returns><see cref="List{Node}"/></returns>
        [OperationContract]
        List<Node> GetAll();

        /// <summary>
        /// Returns the node with the given id
        /// </summary>
        /// <returns><see cref="List{Node}"/></returns>
        [OperationContract]
        Node GetById(string id);

        /// <summary>
        /// Inserts a node into the graph repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        [OperationContract]
        bool Insert(Node node);

        /// <summary>
        /// Updates the given node in the graph repository
        /// </summary>
        /// <param name="node"></param>
        /// <returns>true if operation is successful, false otherwise</returns>
        [OperationContract]
        bool Update(Node node);

        /// <summary>
        /// Deletes the given node from the graph repository
        /// </summary>
        /// <returns>true if operation is successful, false otherwise</returns>
        [OperationContract]
        bool Delete(Node node);

        /// <summary>
        /// Deletes all nodes from graph repository
        /// </summary>
        /// <returns>true if operation is successful, false otherwise</returns>
        [OperationContract]
        bool DeleteAll();

        /// <summary>
        /// Check the status of the service
        /// </summary>
        /// <returns>true if the service is up</returns>
        [OperationContract]
        bool Heartbeat();


    }
}
