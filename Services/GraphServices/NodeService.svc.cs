﻿using System;
using System.Collections.Generic;
using GraphSolver.Core.Domain;
using GraphSolver.Core.Repository;
using log4net;
using System.Configuration;

namespace GraphServices
{
    public class NodeService : INodeService
    {
        private IRepository _repository;
        private ILog _log = LogManager.GetLogger(typeof(NodeService));

        public IRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    // Default to MongoRepository
                    string connectionString = ConfigurationManager.AppSettings["MongoDBConnString"];
                    string databaseName = ConfigurationManager.AppSettings["MongoDBName"];
                    string collectionName = ConfigurationManager.AppSettings["MongoDBCollectionName"];

                    if (!string.IsNullOrEmpty(connectionString) &&
                        !string.IsNullOrEmpty(databaseName) &&
                        !string.IsNullOrEmpty(collectionName))
                    {
                        _repository = new MongoRepository(_log, connectionString, databaseName, collectionName);
                    }
                }
                return _repository;
            }
            set
            {
                _repository = value;
            }
        }

        public bool DeleteAll()
        {
            return Repository.DeleteAll();
        }

        public List<Node> GetAll()
        {
            return Repository.GetAll();
        }

        public bool Heartbeat()
        {
            return true;
        }

        public bool Insert(Node node)
        {
            return Repository.Insert(node);
        }

        public Node GetById(string id)
        {
            return Repository.GetById(id);
        }

        public bool Update(Node node)
        {
            return Repository.Update(node);
        }

        public bool Delete(Node node)
        {
            return Repository.Delete(node);
        }
    }
}
