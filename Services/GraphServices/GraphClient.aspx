﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraphClient.aspx.cs" Inherits="GraphServices.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="Includes/vis.js"></script>
    <link href="Includes/vis.css" rel="stylesheet" type="text/css" />
    <title>Graph Client</title>
    
    <style type="text/css">
        #mynetwork {
            width: 800px;
            height: 600px;
            border: 1px solid lightgray;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Services>
                <asp:ServiceReference Path="~/AjaxNodeService.svc/" />
            </Services>
            
            <Services>
                <asp:ServiceReference Path="~/GraphCalculationService.svc/" />
            </Services>
        </asp:ScriptManager>

    <div>
        <h1>GRAPH CLIENT</h1> 

        <a id="Refresh" href="#" onclick="DrawGraph();" style="font-size: 20px;">Refresh</a> <br/>
        <a id="GetPath" href="#" onclick="GetPath();" style="font-size: 20px;" >Find Shortest Path</a> <span style="font-size: 12px;">(Use "CTRL + mouse" to select two nodes)</span>
        <div id="mynetwork"></div>
        
        
        <script type="text/javascript">

            var network;

            function DrawGraph()
            {
                var service = new window.AjaxNodeService();
                service.GetAll(function (results) {
                    if (!results || results.length < 1) {
                        alert("Repository returned no nodes/graph");
                    }
                    var nodesArr = [];
                    var edgesArr = [];
                    for (var i = 0; i < results.length; i++) {
                        nodesArr.push({
                            id: results[i].Id,
                            label: results[i].Label,
                            shape: 'circle'
                        });

                        for (var j = 0; j < results[i].AdjacentNodes.length; j++) {
                            edgesArr.push({
                                from: results[i].Id,
                                to: results[i].AdjacentNodes[j]
                            });
                        }
                    }

                    var nodes = new vis.DataSet(nodesArr);

                    // create an array with edges
                    var edges = new vis.DataSet(edgesArr);

                    // create a network
                    var container = document.getElementById('mynetwork');
                    var data = {
                        nodes: nodes,
                        edges: edges
                    };
                    var options = {
                        nodes: { borderWidth: 2 },
                        interaction: { hover: true, multiselect: true }
                    }
                    network = new vis.Network(container, data, options);

                });
            }

            function GetPath() {
                var selectedNodes = network.selectionHandler.getSelectedNodes();
                if (selectedNodes.length !== 2) {
                    alert("Please select exactly 2 nodes");
                } else {
                    var grservice = new window.GraphCalculationService();
                    grservice.ShortestPath(selectedNodes[0], selectedNodes[1], function (results) {
                        if (!results || results.length < 1) {
                            alert("Path not found");
                        } else {
                            alert("Found! Path edges are in bold.");
                        }
                        var edgesToSelect = [];
                        for (var i = 0; i < results.length - 1; i++) {
                            for (var key in network.body.edges) {
                                if (network.body.edges.hasOwnProperty(key)) {
                                    if (((network.body.edges[key].fromId === results[i]) && (network.body.edges[key].toId === results[i + 1])) ||
                                        ((network.body.edges[key].toId === results[i]) && (network.body.edges[key].fromId === results[i + 1]))) {
                                        edgesToSelect.push(network.body.edges[key].id);
                                    }
                                }
                            }
                        }
                        network.selectEdges(edgesToSelect);
                    });
                }
            }

            
            function addEvent(element, eventName, fn) {
                if (element.addEventListener)
                    element.addEventListener(eventName, fn, false);
                else if (element.attachEvent)
                    element.attachEvent('on' + eventName, fn);
            }

            addEvent(window, 'load', function () { DrawGraph() });

        </script>

    </div>
    </form>
</body>
</html>
