﻿using GraphSolver.Core.Repository;
using log4net;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using GraphSolver.Core.GraphCalculator;

namespace GraphServices
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GraphCalculationService
    {
        private IRepository _repository;
        private readonly ILog _log = LogManager.GetLogger(typeof(NodeService));

        public IRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    // Default to MongoRepository
                    string connectionString = ConfigurationManager.AppSettings["MongoDBConnString"];
                    string databaseName = ConfigurationManager.AppSettings["MongoDBName"];
                    string collectionName = ConfigurationManager.AppSettings["MongoDBCollectionName"];

                    if (!string.IsNullOrEmpty(connectionString) &&
                        !string.IsNullOrEmpty(databaseName) &&
                        !string.IsNullOrEmpty(collectionName))
                    {
                        _repository = new MongoRepository(_log, connectionString, databaseName, collectionName);
                    }
                }
                return _repository;
            }
            set
            {
                _repository = value;
            }
        }

        /// <summary>
        /// Finds the shortest path between the nodes, ids of which are provided as arguments
        /// </summary>
        /// <returns></returns>
        [WebGet]
        public List<string> ShortestPath(string idFrom, string idTo)
        {
            return Calculator.ShortestPath(Repository.GetAll(), idFrom, idTo);
        }
    }
}
